# README #

### What is this repository for? ###

* Student repository is just a demo project to see how Spring boot and Mongodb works.

### Mongodb instalation ###

* Install [Mongodb](https://www.mongodb.com/try/download/community)
* Install [Mongodb Compass ](https://www.mongodb.com/try/download/compass) (Optional)
* bla mangol install java or Postman x).

### Start Mongodb ###

* run the command : mongodb

![Scheme](images/mongodb.png)

* run your Compass

![Scheme](images/compass.png)


### Required dependencies ###

* All the important dependencies

![Scheme](images/dependencies.png)

### Required properties ###

* Adding such an important info about your database : 

![Scheme](images/application_properties.png)


### Use MongoDB ###

* Add Student to your DB : POST [http://localhost:8081/api/add-student](http://localhost:8081/api/add-student) 

![Scheme](images/post_postman.png)

* Get all Students from your DB : GET [http://localhost:8081/api/all-students](http://localhost:8081/api/all-students) 

![Scheme](images/post_postman.png)

### Explanation Video ###

[Click to watch the video](https://www.youtube.com/watch?v=waRr9RhjuBc) 




package com.student.demo.controllers;

import com.student.demo.models.Student;
import com.student.demo.repositories.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class StudentController {

    private final StudentRepository studentRepository;

    @GetMapping("/all-students")
    public List<Student> getAllStudent(){
        return studentRepository.findAll();
    }

    @PostMapping("/add-student")
    public Student addStudent(@RequestBody Student student){
        return studentRepository.save(student);
    }
}

package com.student.demo.repositories;

import com.student.demo.models.Student;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.webmvc.RepositoryRestController;

@RepositoryRestController
public interface StudentRepository extends MongoRepository<Student,Long> {
}
